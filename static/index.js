'use strict';

// show this login first
var loginModal = new bootstrap.Modal(document.getElementById('loginModal'))
loginModal.show();

let app;
let socket;
let stream;

(async () => {
    stream = await navigator.mediaDevices.getUserMedia({ audio: true });
})();

const login = () => {
    var username = document.getElementById('usernameInput').value

    app = Elm.Main.init({
        node: document.getElementById('myapp'),
        flags: username
    });

    var wsProtocol = window.location.protocol == "https:" ? "wss://" : "ws://";
    var wsPortAppend = window.location.hostname == "localhost" ? ":3000" : "";
    
    socket = new WebSocket(wsProtocol + window.location.hostname + wsPortAppend);

    app.ports.sendMessage.subscribe(function(message) {
        socket.send(message);
    });
   
    socket.addEventListener("message", (event) => {
        console.log(event.data)

        if (JSON.parse(event.data).message) {
            play(event.data);
        } else {
            app.ports.messageReceiver.send(event.data);
        }
    });
    
    socket.addEventListener("open", () => {
        console.log("Opened.")
        socket.send(JSON.stringify({username: username}))
    });
    
    socket.addEventListener("close", () => console.log("Closed."));
}

let mediaRecorder;
let audioChunks = [];
let timerId;

const startRecording = async () => {
    mediaRecorder = new MediaRecorder(stream);
    mediaRecorder.start(50);

    mediaRecorder.addEventListener("dataavailable", event => {
        audioChunks.push(event.data);
    });

    socket.send(JSON.stringify("highlight"))

    timerId = setTimeout(stopRecording, 7000)
}

const stopRecording = async () => {
    clearInterval(timerId);
    if (mediaRecorder.state == "inactive") { return; }

    await mediaRecorder.stop();
    const audioBlob = new Blob(audioChunks);

    const reader = new FileReader();
    reader.readAsDataURL(audioBlob);
    reader.onload = () => {
        const base64AudioMessage = reader.result.split(',')[1];
        var url = "data:audio/webm;base64," + base64AudioMessage;

        const urlString = JSON.stringify({ message: url });
        console.log(urlString);
        socket.send(urlString)

        // reset this
        audioChunks = [];
    };

    socket.send(JSON.stringify("unhighlight"))
}

const play = async (data) => {
    var url = JSON.parse(data).message;
    const audio = new Audio(url);
    await audio.play();
}


document.getElementById('usernameButton').onclick = login;

document.getElementById('recordButton').onmousedown = startRecording;
document.getElementById('recordButton').onmouseup = stopRecording;
