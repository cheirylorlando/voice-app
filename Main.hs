{-# LANGUAGE OverloadedStrings #-}
module Main where

import Control.Concurrent (MVar, newMVar, modifyMVar_, modifyMVar, readMVar)
import Control.Concurrent (forkIO, threadDelay)
import Control.Exception (finally)
import Control.Monad (forever, forM_)

import Data.Aeson
import qualified Data.HashMap.Strict as HM
import Data.Text as T
import Data.UUID (UUID)

import Prelude hiding (Left, Right)

import qualified Network.Wai.Handler.WebSockets as WaiWs
import qualified Network.Wai.Handler.Warp as Warp
import qualified Network.WebSockets as WS

import System.Environment (lookupEnv)
import System.Random (getStdRandom, random)

import qualified Web.Scotty as S

serverApplication = S.scottyApp $ do
    S.get "/" $ do
        S.file "public/index.html"
    S.get "/elm.js" $ do
        S.file "public/elm.js"
    S.get "/index.js" $ do
        S.file "public/index.js"

main :: IO ()
main = do
    port <- maybe 3000 read <$> lookupEnv "PORT" :: IO Int
    let settings = Warp.setPort port Warp.defaultSettings

    webSocketsApp <- wsApplication
    serverApp <- serverApplication

    putStrLn $ "Running on port " <> show port
    Warp.runSettings settings $ WaiWs.websocketsOr WS.defaultConnectionOptions webSocketsApp serverApp



type Location = (Int, Int)
type PlayerInfo = (T.Text, Bool, Location)

data ServerState = ServerState
    { connections :: [(UUID, WS.Connection, PlayerInfo)]
    }

createServerState :: IO ServerState
createServerState = pure $ ServerState
    { connections = []
    }

wsApplication :: IO WS.ServerApp
wsApplication = do
    state <- createServerState
    stateVar <- newMVar state
    pure $ application stateVar

data ServerMessage = GameInformation [PlayerInfo] | VoiceRelay T.Text
instance ToJSON ServerMessage where
    toJSON (GameInformation players) =
        object [
        "players"    .= Prelude.map (\(a,b,(x,y)) -> object
            [ "name" .= a
            , "isHighlighted" .= b
            , "location" .= object [ "x" .= (x :: Int), "y" .= (y :: Int)]
            ]) players
        ]
    toJSON (VoiceRelay contents) =
        object [
        "message" .= contents
        ]

sendServerMessage :: WS.Connection -> ServerMessage -> IO ()
sendServerMessage conn = WS.sendTextData conn . encode

data ClientMessage = GreetClient T.Text | Up | Down | Left | Right | Highlight | UnHighlight | VoiceMessage T.Text
instance FromJSON ClientMessage where
    parseJSON (Object o) = do
        case HM.lookup "message" o of
            Just x  -> do
                msg <- parseJSON x
                return $ VoiceMessage msg
            Nothing -> do
                case HM.lookup "username" o of
                    Just x -> do
                        username <- parseJSON x
                        return $ GreetClient username
                    Nothing -> fail "no username or message"

    parseJSON (String s) = case s of
        "up" -> return Up
        "down" -> return Down
        "left" -> return Left
        "right" -> return Right
        "greet" -> return $ GreetClient "nobody"
        "highlight" -> return Highlight
        "unhighlight" -> return UnHighlight
        _ -> fail "oops"
    parseJSON _ = fail "oops"

receiveClientMessage :: WS.Connection -> IO (Maybe ClientMessage)
receiveClientMessage conn = do
    bsData <- WS.receiveData conn
    pure $ decode bsData

insertConnection :: UUID -> WS.Connection -> T.Text -> ServerState -> IO ServerState
insertConnection uid conn name s = pure
    s{connections = (uid, conn, (name, False, (0,0))) : connections s}

removeConnection :: UUID -> ServerState -> IO ServerState
removeConnection uid s = pure
    s{connections = Prelude.filter (\(x,_,_) -> x /= uid) (connections s)}



updaterThread :: MVar ServerState -> IO ()
updaterThread state = forever $ do
    s <- readMVar state
    let conns = connections s
        players = Prelude.map (\(_,_,x) -> x) conns
        msg = GameInformation players
    forM_ conns (\(_, conn, _) -> sendServerMessage conn msg)

    threadDelay 100000 -- 17000

application :: MVar ServerState -> WS.ServerApp
application state pending = do
    conn <- WS.acceptRequest pending

    _ <- forkIO $ updaterThread state

    WS.withPingThread conn 30 (pure ()) $ do
        msg <- receiveClientMessage conn
        case msg of
            Just (GreetClient name) -> do
                uid <- getStdRandom random :: IO UUID
                let disconnect = modifyMVar_ state (removeConnection uid)
                flip finally disconnect $ do
                    putStrLn "Received first message from client."
                    
                    -- insertConnection handles everything
                    modifyMVar_ state (insertConnection uid conn name)
                    forever $ do
                        msg' <- receiveClientMessage conn
                        case msg' of
                            Just msg'' -> handleClientMessage uid state msg''
                            Nothing -> putStrLn "Client's message couldn't be parsed."
            Just _ -> putStrLn "Unexpected client message type for first message."
            Nothing ->
                putStrLn "Initial message from client couldn't be parsed."
        pure ()

modifyPlayer :: UUID -> (PlayerInfo -> PlayerInfo) -> ServerState -> IO ServerState
modifyPlayer uid f s = pure s{connections = specialMe ++ withoutMe}
    where
        withMe = Prelude.filter (\(x,_,_) -> x == uid) (connections s)
        withoutMe = Prelude.filter (\(x,_,_) -> x /= uid) (connections s)
        specialMe = fmap (\(a,b,c) -> (a, b, f c)) withMe

handleClientMessage :: UUID -> MVar ServerState -> ClientMessage -> IO ()
handleClientMessage uid state msg = do
    case msg of
        GreetClient _ -> putStrLn "Should not receive greet message from client now."
        Left -> do
            modifyMVar_ state $ modifyPlayer uid (\(a,b,(x,y)) -> (a,b,(x-1,y)) )
            putStrLn "Left up"
        Right -> do
            modifyMVar_ state $ modifyPlayer uid (\(a,b,(x,y)) -> (a,b,(x+1,y)) )
            putStrLn "Right up"
        Up -> do
            modifyMVar_ state $ modifyPlayer uid (\(a,b,(x,y)) -> (a,b,(x,y+1)) )
            putStrLn "Up up"
        Down -> do
            modifyMVar_ state $ modifyPlayer uid (\(a,b,(x,y)) -> (a,b,(x,y-1)) )
            putStrLn "Down up"
        Highlight -> do
            modifyMVar_ state $ modifyPlayer uid (\(a,b,c) -> (a,True,c) )
        UnHighlight -> do
            modifyMVar_ state $ modifyPlayer uid (\(a,b,c) -> (a,False,c) )
        VoiceMessage contents -> do
            s <- readMVar state
            let withoutMe = Prelude.filter (\(x,_,_) -> x /= uid) (connections s)
                msg = VoiceRelay contents
            forM_ withoutMe (\(_, conn, _) -> sendServerMessage conn msg)



