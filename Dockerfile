FROM debian:bullseye-slim
RUN apt-get update && apt-get install -y \
    libpq-dev \
    ca-certificates \
  && rm -rf /var/lib/apt/lists/*
RUN mkdir -p /app/user
WORKDIR /app/user
COPY ./bin/server-exe /app/user
COPY ./public/* /app/user/public/
CMD /app/user/server-exe
