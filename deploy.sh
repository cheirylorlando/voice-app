#!/bin/bash

# exit on failure
set -e

# this is a special name specified in Google's documentation
TAG_NAME=$GCR_HOST"/"$PROJECT_ID"/"$IMAGE_NAME

docker build -t $TAG_NAME .
docker push $TAG_NAME
gcloud run deploy $IMAGE_NAME --image=$TAG_NAME --platform managed
