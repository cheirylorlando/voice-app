#!/bin/bash

# exit on failure
set -e

BIN_NAME=server-exe

# build this thing
stack build

# copy the binary somewhere accessible by the Dockerfile
mkdir -p ./bin
cp $(stack path --local-install-root)/bin/$BIN_NAME ./bin/

mkdir -p public
rm -rf public
mkdir -p public

cd elm; elm make src/Main.elm --output ../public/elm.js; cd ../
cp static/* public/
