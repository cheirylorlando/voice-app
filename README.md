# Voice App

1. User can record voice using push-to-talk.
2. User can hear other voice messages.
3. User can move around on screen.
4. User can "alert" by changing a different color.

## Implementation constraints
1. Uses Bootstrap
2. Uses `elm/svg` for 2d graphics.
3. Uses `scotty` and websockets.
4. Is deployable on GCP.
5. Uses Base64 encoded audio files.

## Issues
1. No boundary!
2. Doesn't work on mobile.
3. People aren't used to push to talk.

## Variables
`GCR_HOST` (e.g. gcr.io)
- The Cloud Registry host based on region

`PROJECT_ID` (e.g. soccer-mystic-392871)
- The GCP project id associated with the project

`IMAGE_NAME` (e.g. rooms-server)
- What you want to name the image

