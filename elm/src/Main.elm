port module Main exposing (..)

import Browser
import Html exposing (..)
import Html.Events exposing (onClick)

import Svg exposing (..)
import Svg.Attributes exposing (..)

import Json.Decode as Decode exposing (..)
import Browser.Events exposing (onKeyDown, onKeyUp)
main =
  Browser.element
    { init = init
    , view = view
    , update = update
    , subscriptions = subscriptions
    }

type alias Location = { x: Int, y: Int }
type alias Player = { name: String, isHighlighted: Bool, location: Location }
type alias Model = { username: String, audioQueue: List String, players: List Player }

port sendMessage : String -> Cmd msg
port messageReceiver : (String -> msg) -> Sub msg

somePlayers =
    [ { name = "ryan", isHighlighted = False, location = { x = 3, y = 5 }}
    , { name = "cars", isHighlighted = True, location = { x = 5, y = 2 }}
    , { name = "mirs", isHighlighted = False, location = { x = 2, y = 1 }}
    ]

init : String -> (Model, Cmd Msg)
init username =
  ( { username = username, audioQueue = [], players = somePlayers}
  , Cmd.none
  )


type Msg
  = Update String
  | Up
  | Down
  | Left
  | Right
  | Highlight
  | UnHighlight
  | Other

getCommand : Msg -> Cmd Msg
getCommand msg = case msg of
    Up -> sendMessage "\"up\""
    Down -> sendMessage "\"down\""
    Left -> sendMessage "\"left\""
    Right -> sendMessage "\"right\""
    Highlight -> sendMessage "\"highlight\""
    UnHighlight -> sendMessage "\"unhighlight\""
    _ -> Cmd.none
  
decodePlayers =
  field "players" (list (map3 Player
    (field "name" Decode.string)
    (field "isHighlighted" bool)
    (field "location" (map2 Location (field "x" int) (field "y" int)))
  ))


update : Msg -> Model -> (Model, Cmd Msg)
update msg model = case msg of
    Update str ->
      ( case decodeString decodePlayers str of
         Ok players -> { model | players = players }
         Err _ -> model
      , Cmd.none
      )
    _ -> (model, getCommand msg)

keyDownDecoder : Decode.Decoder Msg
keyDownDecoder =
  Decode.map keyDownMap (Decode.field "key" Decode.string)
keyDownMap : String -> Msg
keyDownMap string =
  case string of
    "ArrowUp" -> Up
    "ArrowDown" -> Down
    "ArrowRight" -> Right
    "ArrowLeft" -> Left
    -- "h" -> Highlight
    _ -> Other

keyUpDecoder : Decode.Decoder Msg
keyUpDecoder =
  Decode.map keyUpMap (Decode.field "key" Decode.string)
keyUpMap : String -> Msg
keyUpMap string =
  case string of
    -- "h" -> UnHighlight
    _ -> Other

subscriptions : Model -> Sub Msg
subscriptions model = Sub.batch
    [ onKeyDown keyDownDecoder
    , onKeyUp keyUpDecoder
    , messageReceiver Update
    ]

viewBackground = rect
  [ width "100%"
  , height "100%"
  , fill "whitesmoke"
  ]
  []

-- need to add a name tag
viewCharacter c =
    [ rect
        [ x (String.fromInt (c.location.x * 50))
        , y (String.fromInt (550 - c.location.y * 50))
        , width "50"
        , height "50"
        , if c.isHighlighted then fill "deeppink" else fill "powderblue"
        ]
        []
    , text_
        [ x (String.fromInt (c.location.x * 50))
        , y (String.fromInt (550 - c.location.y * 50))
        , fill "black"
        ]
        [ Svg.text c.name ]
    ]

view : Model -> Html Msg
view model =
  div []
    [
      svg [ width "800", height "600"]
      ( viewBackground ::
      (List.concatMap viewCharacter model.players)
      )
    ]
